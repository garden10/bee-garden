﻿using System;
using UnityEngine;

namespace BeeGarden.Model 
{
    /// <summary>
    /// Абстрактный класс пчел, от которого наследуются рабочая пчела и трутень
    /// </summary>
    public abstract class Bee : IUpdatable
    {
        /// <summary>
        /// Родной улей
        /// </summary>
        [NonSerialized] protected Beehive beehive;
        
        /// <summary>
        /// Идентификатор пчелы
        /// </summary>
        protected int arrayIndex;

        /// <summary>
        /// Скорость пчелы
        /// </summary>
        protected float speed; 
        
        /// <summary>
        /// Позиция пчелы
        /// </summary>
        protected Vector3 position;

        /// <summary>
        /// Направление движения пчелы
        /// </summary>
        protected Vector3 direction = Vector3.forward;
        
        public Vector3 Position => position;
        public Vector3 Direction => direction;
        
        public int ArrayIndex
        {
            get => arrayIndex;
            set => arrayIndex = value;
        }

        /// <summary>
        /// Родной улей
        /// </summary>
        public Beehive Beehive
        {
            get => beehive;
            set => beehive = value;
        }

        public Bee(Beehive _beehive, int _arrayIndex, float _speed, Vector3 _position)
        {
            arrayIndex = _arrayIndex;
            speed = _speed;
            position = _position;
            Beehive = _beehive;
        }

        public virtual void Update(float _deltaTime)
        {
            
        }

        public abstract void HideViewComponent();
    }
}
