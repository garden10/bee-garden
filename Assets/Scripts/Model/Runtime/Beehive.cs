﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BeeGarden.Model
{
    /// <summary>
    /// Класс улья
    /// </summary>
    [Serializable]
    public class Beehive : IUpdatable
    {
        #region Params
        
        private Field field;
        
        [Header("Настройки для пчелы")] 
        
        [SerializeField] private float beeSpeed;
        
        [SerializeField] private float workerMaxCapacityHoney;
        
        [SerializeField] private float workerCollectionSpeed;
        
        [SerializeField] private float workerFlowerDistance;

        [Header("Настройки для улья")] 
        
        [SerializeField] private float maxHoneyCapacity;
        
        [SerializeField] private int maxBeeCount;
        
        [SerializeField] private int startBeeCount;
        
        [SerializeField] private float newBornTime;
        
        [SerializeField] private float beeDieTime;

        /// <summary>
        /// Список всех рабочих
        /// </summary>
        private List<Bee> beeList = new List<Bee>(512);   

        /// <summary>
        /// Список всех трутней
        /// </summary>
        private List<Drone> droneList = new List<Drone>(512); 
        
        private float currentHoneyInHive = 0;

        /// <summary>
        /// Вероятность рождения трутня
        /// </summary>
        [SerializeField] private float droneCoefficient;

        /// <summary>
        /// На каком этапе заполнения улья будет включаться условие повышенной смертности трутней
        /// </summary>
        [SerializeField] private float droneGenocideCoefficient;

        /// <summary>
        /// Активно условие при котором первыми будут умирать трутни
        /// </summary>
        private bool droneGenocide; 

        private Vector3 position;
        
        /// <summary>
        /// Счетчики рождения и смерти
        /// </summary>
        private float bornCounter = 0;
        private float dieCounter = 0;
        
        /// <summary>
        /// Счетчики пчел
        /// </summary>
        private int workersCounter;   
        private int dronesCounter;
        
        #endregion

        #region Properties

        public Vector3 Position => position;
        
        public float HoneyInHive => currentHoneyInHive;
        
        public float HoneyMax => maxHoneyCapacity;

        public int BeeCount => beeList.Count;
        
        public int BeeCountMax => maxBeeCount;
        
        public int WorkersCount => beeList.Count - droneList.Count;
        
        public int DronesCount => droneList.Count;

        #endregion
        
        #region Events

        /// <summary>
        /// Рождение рабочего
        /// </summary>
        public event Action<Worker> OnWorkerOut;

        /// <summary>
        /// Рождение трутня
        /// </summary>
        public event Action<Drone> OnDroneOut;

        public event Action<float> TankIsFull;

        #endregion

        #region Methods

        /// <summary>
        /// Инициализация пчел, которые изначально будут в улье
        /// </summary>
        public void InitBees()
        {
            for (int i = 0; i < startBeeCount; i++)
                CreateNewBee();
        }

        public Beehive(Field _field, float _speed, float _workerMaxCapacityHoney,
            float _workerCollectionSpeed, float _workerFlowerDistance, Vector3 _position,
            float _maxHoneyCapacity, int _maxBeeCount, int _startBeeCount, float _newBornTime, float _beeDieTime,
            float _droneCoefficient, float _droneGenocideCoefficient)
        {
            field = _field;
            beeSpeed = _speed;
            workerMaxCapacityHoney = _workerMaxCapacityHoney;
            workerCollectionSpeed = _workerCollectionSpeed;
            workerFlowerDistance = _workerFlowerDistance;
            position = _position;
            maxHoneyCapacity = _maxHoneyCapacity;
            maxBeeCount = _maxBeeCount;
            startBeeCount = _startBeeCount;
            newBornTime = _newBornTime;
            beeDieTime = _beeDieTime;
            droneCoefficient = _droneCoefficient;
            droneGenocideCoefficient = _droneGenocideCoefficient;
        }

        /// <summary>
        /// Передача меда от рабочей пчелы
        /// </summary>
        /// <param name="_worker">Рабочая пчела</param>
        /// <returns>Возвращает число меда, забранное ульем</returns>
        public float HoneyImport(Worker _worker)
        {
            if (currentHoneyInHive < maxHoneyCapacity)
            {
                var hiveDifference = maxHoneyCapacity - currentHoneyInHive;
                var workerDifferrence = _worker.CurrentHoney - _worker.CollectionSpeed;
                
                if (_worker.CollectionSpeed < hiveDifference)
                {
                    currentHoneyInHive += workerDifferrence > 0 ? _worker.CollectionSpeed : _worker.CurrentHoney;
                    return workerDifferrence > 0 ? _worker.CollectionSpeed : _worker.CurrentHoney;
                }

                currentHoneyInHive = maxHoneyCapacity;
                return hiveDifference;
            }
            return 0;
        }

        public void Update(float _deltaTime)
        {
            BeeBirth(_deltaTime);
            
            BeeDeath(_deltaTime);

            BeeControl(_deltaTime);
        }

        private void BeeBirth(float _deltaTime)
        {
            if (bornCounter < newBornTime) // рождение
                bornCounter += _deltaTime;
            else
            {
                CreateNewBee();
                bornCounter -= newBornTime;
            }
        }

        private void BeeDeath(float _deltaTime)
        {
            var currentDieTime = droneGenocide ? newBornTime / 2 : beeDieTime;

            if (dieCounter < currentDieTime) //смерть
                dieCounter += _deltaTime;
            else
            {
                if (droneGenocide && droneList.Count != 0) // при доп условии для смерти пчел, в первую очередь уничтожаются трутни
                {
                    var victimNumber = Random.Range(0, droneList.Count);
                    droneList[victimNumber].HideViewComponent();
                    DeleteDrone(droneList[victimNumber]);
                    dieCounter = 0;
                }
                else if (beeList.Count > 0)
                {
                    var victimNumber = Random.Range(0, beeList.Count);
                    beeList[victimNumber].HideViewComponent();

                    if (beeList[victimNumber] is Drone)
                        DeleteDrone((Drone) beeList[victimNumber]);
                    else
                        DeleteBee(beeList[victimNumber]);
                    dieCounter -= currentDieTime;
                }
            }
        }

        private void BeeControl(float _deltaTime)
        {
            for (int i = beeList.Count - 1; i >= 0; i--) //управление пчелами
            {
                beeList[i].Update(_deltaTime);
                if (beeList[i] is Worker && !IsFullHoney()) //Если работяга и улей неполон, то отправить за медом
                {
                    var worker = (Worker) beeList[i];
                    if (worker.IsReadyToHarvest)
                    {
                        worker.GoToWork();
                        OnWorkerOut?.Invoke(worker);
                    }
                }
                else if (beeList[i] is Drone) //Отправка трутней чисто почилиться
                {
                    var drone = (Drone) beeList[i];
                    if (drone.IsReadyToWalk)
                    {
                        drone.GoToWalk();
                        OnDroneOut?.Invoke(drone);
                    }
                }
            }
        }


        public bool IsFullHoney() =>
            currentHoneyInHive >= maxHoneyCapacity;

        /// <summary>
        /// Отдать накопленный мед
        /// </summary>
        /// <returns>Возвращает полную баночку меда</returns>
        public float GiveHoney()
        {
            if (IsFullHoney())
            {
                var givenHoney = currentHoneyInHive;
                currentHoneyInHive = 0;
                return givenHoney;
            }
            return 0;
        }

        private void CreateNewBee()
        {
            var currentCoefficient = Random.value;
            if (beeList.Count < maxBeeCount)
            {
                if (currentCoefficient <= droneCoefficient)
                    CreateNewDrone();
                else
                    CreateNewWorker();
                droneGenocide = droneGenocide && !(beeList.Count / (float) maxBeeCount < droneGenocideCoefficient);
            }
            else
                droneGenocide = true;    // При переполнении улья включается дополнительно условие для смерти пчел
        }
        
        private void CreateNewWorker()
        {
            var worker = new Worker(this, beeList.Count, beeSpeed, workerMaxCapacityHoney,
                workerCollectionSpeed, position, workerFlowerDistance);
            worker.InitializeFSM();
            beeList.Add(worker);
            workersCounter++;
        }
        
        private void CreateNewDrone()
        {
            var drone = new Drone(field, this, beeList.Count, droneList.Count, beeSpeed, position);
            beeList.Add(drone);
            droneList.Add(drone);
            dronesCounter++;
        }
        
        private void DeleteBee(Bee _bee)
        {
            if (_bee is Worker)
            {
                var worker = (Worker) _bee;
                worker.WorkerDie();
            }

            var last = beeList.Count - 1;
            var current = _bee.ArrayIndex;
            if (current != last)
            {
                beeList[current] = beeList[last];
                beeList[current].ArrayIndex = current;
            }
            beeList.RemoveAt(last);
        }
        
        private void DeleteDrone(Drone _drone)
        {
            var last = droneList.Count - 1;
            var current = _drone.DroneArrayIndex;
            if (current != last)
            {
                droneList[current] = droneList[last];
                droneList[current].DroneArrayIndex = current;
            }
            droneList.RemoveAt(last);
            DeleteBee(_drone);
        }

        public int GetWorkersCount() =>
            workersCounter;

        public int GetDronesCount() =>
            dronesCounter;

        #endregion
    }
}