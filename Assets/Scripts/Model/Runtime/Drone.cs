﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BeeGarden.Model
{
    /// <summary>
    /// Класс трутней
    /// </summary>
    [Serializable]
    public class Drone : Bee
    {
        private enum State
        {
            InHive,
            FlyToWalk
        }

        private Field field;
        
        /// <summary>
        /// Идентификатор трутня. Используется независимо от общего индикатора пчел
        /// </summary>
        private int droneArrayIndex;
        
        /// <summary>
        /// Счетчик времени
        /// </summary>
        private float timeCounter = 1;
        
        /// <summary>
        /// Случайный вектор, куда будет напрвлен трутень
        /// </summary>
        private Vector3 randomVector = Vector3.zero;

        /// <summary>
        /// Время для переориентировкии трутня
        /// </summary>
        private float randomTime = 0;
        
        [SerializeField]
        private State currentState;

        public int DroneArrayIndex
        {
            get => droneArrayIndex;
            set => droneArrayIndex = value;
        }

        public bool IsReadyToWalk => currentState == State.InHive;

        public Drone(Field _field, Beehive _beehive, int _arrayIndex, int _droneArrayIndex, float _speed, Vector3 _position)
            : base(_beehive, _arrayIndex, _speed, _position)
        {
            currentState = State.InHive;
            field = _field;
            droneArrayIndex = _droneArrayIndex;
        }

        /// <summary>
        /// Скрыть визуальный компонент пчелы
        /// </summary>
        public event Action<Drone> HideView;

        /// <summary>
        /// Команда от улья вылететь из него
        /// </summary>
        public void GoToWalk()
        {
            currentState = State.FlyToWalk;
        }

        /// <summary>
        /// Движение трутня
        /// </summary>
        /// <param name="_deltaTime"></param>
        private void FlyToWalk(float _deltaTime)
        {
            
            if (timeCounter < randomTime)
                timeCounter += _deltaTime;
            else
                RandomizeTarget();
            if (Vector3.SqrMagnitude(position - randomVector) > 1f)
                position += direction * speed * _deltaTime;
            else
                RandomizeTarget();
            
        }

        /// <summary>
        /// Установление случайной цели для трутня
        /// </summary>
        private void RandomizeTarget()
        {
            timeCounter -= 1;
            randomVector = new Vector3(Random.Range(field.Border, field.Size.x - field.Border), 0, Random.Range(field.Border, field.Size.y - field.Border)); 
            randomTime = Random.Range(0, 3);
            direction = (randomVector - position).normalized;
        }

        public override void Update(float _deltaTime)
        {
            if(currentState == State.FlyToWalk)
                FlyToWalk(_deltaTime);
        }

        public override void HideViewComponent()
        {
            HideView?.Invoke(this);
        }
    }
}
