﻿namespace Model.Runtime.Services
{
    public interface IStatistic : IService
    {
        void SaveTime(float timeCounter);
        void SaveData(string dataKey, int data);
    }
}