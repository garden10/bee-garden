﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace BeeGarden.Model
{
    /// <summary>
    /// Класс управления ульями 
    /// </summary>
    [Serializable]
    public class BeehiveInitializer
    {
        #region Params

        /// <summary>
        /// Скорость пчелы
        /// </summary>
        [Header("Настройки для пчелы")]
        [SerializeField] private float beeSpeed;
        
        /// <summary>
        /// Максимальное количество уносимого меда рабочим
        /// </summary>
        [SerializeField] private float workerMaxCapacityHoney;

        /// <summary>
        /// Скорость сбора меда рабочим
        /// </summary>
        [SerializeField] private float workerCollectionSpeed;

        /// <summary>
        /// Расстояние, на которое рабочий подлетает к цветку
        /// </summary>
        [SerializeField] private float workerFlowerDistance;

        /// <summary>
        /// Максимальная емкость меда в улье
        /// </summary>
        [Header("Настройки для улья")] [SerializeField]
        private float hiveMaxHoneyCapacity;

        /// <summary>
        /// Максимальное число пчел в улье
        /// </summary>
        [SerializeField] private int hiveMaxBeeCount;

        /// <summary>
        /// Начальное количество пчел в улье
        /// </summary>
        [SerializeField] private int hiveStartBeeCount;

        /// <summary>
        /// Время, через которое родится новая пчела
        /// </summary>
        [SerializeField] private float hiveNewBornTime;

        /// <summary>
        /// Время, через которое умрет старая пчела
        /// </summary>
        [SerializeField] private float hiveDieTime;

        /// <summary>
        /// Вероятность рождения трутня
        /// </summary>
        [Range(0, 1)] [SerializeField] private float hiveDroneCoefficient = 0.2f;
        
        /// <summary>
        /// На каком этапе заполнения улья будет включаться условие повышенной смертности трутней
        /// </summary>
        [Range(0, 1)] [SerializeField] private float hiveDroneGenocideCoefficient = 0.8f;
        
        /// <summary>
        /// Сколько всего будет ульев
        /// </summary>
        [Header("Настройки для размещения ульев")] 
        [SerializeField] private int hiveCount;
        
        /// <summary>
        /// Позиция улья на пасеке
        /// </summary>
        private Vector3 position;
        
        private Field field;
        private Beehive beehive;

        /// <summary>
        /// Счетчик ульев
        /// </summary>
        private int hiveCounter;
        
        #endregion
        
        public event Action<Beehive, int> BeeHiveBorn;

        #region Methods
        
        public void Initialize(Field _field, int _hiveCount, float _hiveMaxHoney,
            int _hiveMaxBee, int _hiveStartBee, float _hiveBornTime, float _hiveDieTime,
            float _hiveDroneCoefficient, float _hivedroneGenocideCoefficient, float _beeSpeed,
            float _workerCapacity, float _workerCollectionSpeed, float _workerFlowerDistance)
        {
            field = _field;
            hiveCount = _hiveCount;
            hiveMaxHoneyCapacity = _hiveMaxHoney;
            hiveMaxBeeCount = _hiveMaxBee;
            hiveStartBeeCount = _hiveStartBee;
            hiveNewBornTime = _hiveBornTime;
            hiveDieTime = _hiveDieTime;
            hiveDroneCoefficient = _hiveDroneCoefficient;
            hiveDroneGenocideCoefficient = _hivedroneGenocideCoefficient;
            beeSpeed = _beeSpeed;
            workerMaxCapacityHoney = _workerCapacity;
            workerCollectionSpeed = _workerCollectionSpeed;
            workerFlowerDistance = _workerFlowerDistance;

            for (int i = 0; i < hiveCount; i++)
            {
                SetPosition();
                beehive = CreateHives();
                BeeHiveBorn?.Invoke(beehive, i);
            }
        }

        /// <summary>
        /// Создание новых ульев
        /// </summary>
        /// <returns></returns>
        public Beehive CreateHives()
        {
            var hive = new Beehive(field, beeSpeed, workerMaxCapacityHoney, workerCollectionSpeed,
                workerFlowerDistance, position, hiveMaxHoneyCapacity, hiveMaxBeeCount, hiveStartBeeCount, hiveNewBornTime, hiveDieTime,
                hiveDroneCoefficient, hiveDroneGenocideCoefficient);
            hiveCounter++;
            return hive;
        }

        public int GetHiveCount() =>
            hiveCounter;

        /// <summary>
        /// Случайная расстановка ульев на пасеке
        /// </summary>
        private void SetPosition()
        {
            position = new Vector3(Random.Range(0, field.Size.x), 0, Random.Range(0, field.Size.y));
        }

        #endregion
    }
}