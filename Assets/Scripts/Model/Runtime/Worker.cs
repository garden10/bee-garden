﻿using System;
using System.Collections.Generic;
using Model.Runtime.Services;
using Model.Runtime.WorkerFSM;
using Model.Runtime.WorkerFSM.States;
using UnityEngine;

namespace BeeGarden.Model
{
    /// <summary>
    /// Класс рабочих пчел
    /// </summary>
    [Serializable]
    public class Worker : Bee
    {
        #region Params
        
        [SerializeField] private float maxCapacityHoney;
        
        [SerializeField] private float collectionSpeed;
        
        [SerializeField] private float currentHoney;    
        
        [SerializeField] private float reachFlowerDistance = 0.1f;

        [NonSerialized] private Flower currentFlower;
        
        private WorkerFSM workerFSM;

        private InHive inHiveState;
        private InFlower inFlowerState;
        private FlyToHive flyToHiveState;
        private FlyToFlower flyToFlowerState;
        private RestInPeace restInPeaceState;

        #endregion

        #region Properties
        
        public bool IsEmptyTank => CurrentHoney <= 0;

        public bool IsReadyToHarvest => IsEmptyTank && workerFSM.CurrentState == inHiveState && AllServices.Container.Single<IFlowerManager>().GetFlowerList().Count != 0; 

        public float CollectionSpeed => collectionSpeed;
        
        public Vector3 Direction 
        {
            get => direction;
            set => direction = value;
        }

        public Vector3 Position
        {
            get => position;
            set => position = value;
        }
        public float Speed
        {
            get => speed;
            set => speed = value;
        }


        public Flower CurrentFlower
        {
            get => currentFlower;
            set => currentFlower = value;
        }

        public WorkerFSM WorkerFsm
        {
            get => workerFSM;
            set => workerFSM = value;
        }


        public float ReachFlowerDistance
        {
            get => reachFlowerDistance;
            set => reachFlowerDistance = value;
        }

        public float CurrentHoney
        {
            get => currentHoney;
            set => currentHoney = value;
        }

        public float MAXCapacityHoney
        {
            get => maxCapacityHoney;
            set => maxCapacityHoney = value;
        }

        #endregion

        #region Events
        
        /// <summary>
        /// Скрыть визуальный компонент пчелы
        /// </summary>
        public event Action<Worker> HideView;

        public event Action<float> HoneyImport;
        
        #endregion
        
        #region Methods
        
        public Worker(Beehive _beehive, int _arrayIndex, float _speed, float _maxCapacityHoney, float _collectionSpeed, Vector3 _position, float reachFlowerDistance)
            : base(_beehive, _arrayIndex, _speed, _position)
        {
            MAXCapacityHoney = _maxCapacityHoney;
            collectionSpeed = _collectionSpeed;
            ReachFlowerDistance = reachFlowerDistance;
        }
        
        public void GoToWork() =>
            workerFSM.MoveNext(WorkerFSMComand.FlyToFlower);

        public void HoneyProgressBar() =>
            HoneyImport?.Invoke(CurrentHoney/MAXCapacityHoney);

        public override void HideViewComponent() =>
            HideView?.Invoke(this);

        public override void Update(float _deltaTime)
        {
            if (workerFSM != null)
                workerFSM.Update(_deltaTime);
        }

        public void InitializeFSM()
        {
            inHiveState = new InHive();
            inHiveState.Worker = this;
            
            inFlowerState = new InFlower();
            inFlowerState.Worker = this;
            
            flyToHiveState = new FlyToHive();
            flyToHiveState.Worker = this;
            
            flyToFlowerState = new FlyToFlower();
            flyToFlowerState.Worker = this;

            restInPeaceState = new RestInPeace();

            var transitions = new Dictionary<WorkerStateTransition, IWorkerState>
            {
                {new WorkerStateTransition(inHiveState, WorkerFSMComand.FlyToFlower), flyToFlowerState},
                {new WorkerStateTransition(inHiveState, WorkerFSMComand.Die), restInPeaceState},
                {new WorkerStateTransition(flyToFlowerState, WorkerFSMComand.FlyToHive), flyToHiveState},
                {new WorkerStateTransition(flyToFlowerState, WorkerFSMComand.InFlower), inFlowerState},
                {new WorkerStateTransition(flyToFlowerState, WorkerFSMComand.Die), restInPeaceState},
                {new WorkerStateTransition(inFlowerState, WorkerFSMComand.FlyToFlower), flyToFlowerState},
                {new WorkerStateTransition(inFlowerState, WorkerFSMComand.FlyToHive), flyToHiveState},
                {new WorkerStateTransition(inFlowerState, WorkerFSMComand.Die), restInPeaceState},
                {new WorkerStateTransition(flyToHiveState, WorkerFSMComand.InHive), inHiveState},
                {new WorkerStateTransition(flyToHiveState, WorkerFSMComand.Die), restInPeaceState}
            };
            workerFSM = new WorkerFSM();
            workerFSM.Initialize(transitions, inHiveState);
        }

        public void WorkerDie()
        {
            workerFSM.MoveNext(WorkerFSMComand.Die);
        }
        #endregion

    }
}
