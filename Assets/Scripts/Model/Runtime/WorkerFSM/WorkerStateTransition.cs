﻿namespace Model.Runtime.WorkerFSM
{
    public class WorkerStateTransition
    {
        private readonly IWorkerState currentState;
        private readonly WorkerFSMComand command;

        public WorkerStateTransition(IWorkerState _currentState, WorkerFSMComand _command)
        {
            currentState = _currentState;
            command = _command;
        }

        public override int GetHashCode()
        {
            return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
        }

        public override bool Equals(object _obj)
        {
            var other = _obj as WorkerStateTransition;
            return other != null && this.currentState == other.currentState && this.command == other.command;
        }
    }
}