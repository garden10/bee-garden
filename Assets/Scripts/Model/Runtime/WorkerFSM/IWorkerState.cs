﻿namespace Model.Runtime.WorkerFSM
{
    public interface IWorkerState
    {
        void Update(float _deltaTime);
        
        void Enter();
        
        void Exit();
    }
}