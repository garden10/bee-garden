﻿namespace Model.Runtime.WorkerFSM
{
    public enum WorkerFSMComand
    {
        InHive,
        FlyToFlower,
        InFlower,
        FlyToHive,
        Die
    }
}