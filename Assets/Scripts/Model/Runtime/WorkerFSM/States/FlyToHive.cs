﻿using BeeGarden.Model;
using UnityEngine;

namespace Model.Runtime.WorkerFSM.States
{
    public class FlyToHive : IWorkerState
    {
        private float timeCounter;
        private Worker worker;

        public Worker Worker
        {
            get => worker;
            set => worker = value;
        }
        
        public void Update(float _deltaTime)
        {
            worker.Direction = (worker.Beehive.Position - worker.Position).normalized;
            worker.Position += worker.Direction * worker.Speed * _deltaTime;
            
            if (Vector3.Distance(worker.Beehive.Position, worker.Position) < worker.ReachFlowerDistance)
            {
              worker.WorkerFsm.MoveNext(WorkerFSMComand.InHive);
              worker.HideViewComponent();
            }
        }

        public void Enter()
        {
        }

        public void Exit()
        {
        }
    }
}