﻿using BeeGarden.Model;

namespace Model.Runtime.WorkerFSM.States
{
    public class InHive : IWorkerState
    {
        private float timeCounter;
        private Worker worker;

        public Worker Worker
        {
            get => worker;
            set => worker = value;
        }
        
        public void Update(float _deltaTime)
        {
            if (worker.CurrentHoney > 0)
            {
                if (timeCounter < 1)
                    timeCounter += _deltaTime;
                else
                {
                    timeCounter -= 1;
                    worker.CurrentHoney -= worker.Beehive.HoneyImport(worker);
                }
            }
            
        }

        public void Enter()
        {
            
        }

        public void Exit()
        {

        }
    }
}