﻿using BeeGarden.Model;
using Model.Runtime.Services;
using UnityEngine;

namespace Model.Runtime.WorkerFSM.States
{
    public class FlyToFlower : IWorkerState
    {
        private Worker worker;

        public Worker Worker
        {
            get => worker;
            set => worker = value;
        }

        public void Update(float _deltaTime)
        {
            if (worker.CurrentFlower != null)
            {
                worker.Direction = (worker.CurrentFlower.Position - worker.Position).normalized;
                worker.Position += worker.Direction * worker.Speed * _deltaTime;
                
                if (Vector3.Distance(worker.CurrentFlower.Position, worker.Position) < worker.ReachFlowerDistance)
                    worker.WorkerFsm.MoveNext(WorkerFSMComand.InFlower);
            }
        }

        public void Enter()
        {
            FindNewFlower();
        }

        public void Exit()
        {
            if(worker.CurrentFlower != null)
                worker.CurrentFlower.OnDie -= OnFlowerDie;
        }

        private void FindNewFlower()
        {
            if (worker.CurrentFlower == null)
            {
                if (AllServices.Container.Single<IFlowerManager>().GetFlowerList().Count != 0)
                {
                    var flowerPurpose = Random.Range(0, AllServices.Container.Single<IFlowerManager>().GetFlowerList().Count);
                    worker.CurrentFlower = AllServices.Container.Single<IFlowerManager>().GetFlowerList()[flowerPurpose];
                    worker.CurrentFlower.OnDie += OnFlowerDie;
                }
                else
                    worker.WorkerFsm.MoveNext(WorkerFSMComand.FlyToHive);
            }
        }

        private void OnFlowerDie(Flower _flower)
        {
            worker.CurrentFlower.OnDie -= OnFlowerDie;
            worker.CurrentFlower = null;
            FindNewFlower();
        }
    }
}