﻿using BeeGarden.Model;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления в юнити класса трутня
    /// </summary>
    public class DroneView : MonoBehaviour
    {
        [SerializeField] private Drone drone;
        
        public void Initialize(Drone _drone)
        {
            drone = _drone;
            transform.position = _drone.Position;
            drone.HideView += HideView;
        }

        private void Update()
        {
            transform.position = drone.Position;
            transform.rotation = Quaternion.LookRotation(drone.Direction);
        }
        
        private void HideView(Drone _drone)
        {
            if (_drone != drone) return;    
            _drone.HideView -= HideView;
            gameObject.SetActive(false);
        }
    }
}
