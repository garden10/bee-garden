﻿using System;
using BeeGarden.Model;
using Model.Runtime.Services;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс, отвечающий за статистику
    /// </summary>
    public class StatisticManagerView : MonoBehaviour
    {
        [SerializeField] private StatisticManager statisticManager;
        public static event Action GetData;
        
        private float timeCounter;
        private int sessionCounter;

        private void Awake() =>
            statisticManager.Initialize();

        /// <summary>
        /// Подключается к кнопке старта игры
        /// </summary>
        public void StartCounter() =>
            timeCounter = Time.realtimeSinceStartup;

        /// <summary>
        /// Подключается к кнопкам переключения сцен, сохраняет все данные в игре
        /// </summary>
        public void SaveResults()
        {
            GetData?.Invoke();
            timeCounter = timeCounter == 0 ? 0 : Time.realtimeSinceStartup - timeCounter; 
            AllServices.Container.Single<IStatistic>().SaveTime(timeCounter);
            AllServices.Container.Single<IStatistic>().SaveData("SessionCount", 1);
        }
    }
}