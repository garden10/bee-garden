﻿using System;
using BeeGarden.Model;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления полюшка в Unity
    /// </summary>
    [Serializable]
    public class FieldView : MonoBehaviour
    {
        [SerializeField] private Field field;

        public Field Field => field;

        private void Start()
        {
            transform.localPosition = new Vector3(field.Size.x * 0.5f, 0, field.Size.y * 0.5f);
            transform.localScale = new Vector3(field.Size.x + field.Border, 1,field.Size.y + field.Border);
        }
    }
}