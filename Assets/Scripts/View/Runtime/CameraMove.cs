﻿using UnityEngine;

/// <summary>
/// Класс камеры в игре
/// </summary>
public class CameraMove : MonoBehaviour
{
    private Vector3 сursor;

    [SerializeField] private Camera mainCamera;

    /// <summary>
    /// Коэффициент, показывающий, на какой части экрана с краю камера начинает реагировать на курсор 
    /// </summary>
    private int borderCoefficient = 12;

    /// <summary>
    /// Обозначение границ экрана, переходя которые камера начинает вращаться
    /// </summary>
    private int xCursorRight;
    private int xCursorLeft;
    private int yCursorUp;
    private int yCursorDown;

    /// <summary>
    /// Ограничители по высоте для камеры
    /// </summary>
    private int borderUp = 85;
    private int borderDown = 5;

    /// <summary>
    /// Границы для при ближения колесиком мыши
    /// </summary>
    private int leftToCenterBorder = -85;
    private int rightToCenterBorder = -35;
    private int leftFromCenterBorder = -30;
    private int rightFromCenterBorder = -80;

    private void Start()
    {
        xCursorLeft = Screen.width / borderCoefficient;
        xCursorRight = Screen.width - Screen.width / borderCoefficient;
        yCursorUp = Screen.height / borderCoefficient;
        yCursorDown = Screen.height - Screen.height / borderCoefficient;
    }

    void FixedUpdate()
    {
        сursor = Input.mousePosition;
        Moving(); //движение камеры за курсором
    }
    private void Moving()
    {
        if (сursor.x < xCursorLeft)
            gameObject.transform.RotateAround(transform.position, Vector3.up, 1);

        if (сursor.x > xCursorRight)
            gameObject.transform.RotateAround(transform.position, Vector3.up, -1);

        if (сursor.y > yCursorUp && transform.localEulerAngles.x < borderUp)
            gameObject.transform.Rotate(Vector3.right, Space.Self);

        if (сursor.y < yCursorDown && transform.localEulerAngles.x > borderDown)
            gameObject.transform.Rotate(Vector3.left, Space.Self);

        var cameraPosition = mainCamera.gameObject.transform.localPosition.z;

        if (cameraPosition > leftToCenterBorder && cameraPosition < rightToCenterBorder && Input.mouseScrollDelta.y > 0)
            mainCamera.gameObject.transform.localPosition += new Vector3(0, 0, Input.mouseScrollDelta.y);

        if (cameraPosition < leftFromCenterBorder && cameraPosition > rightFromCenterBorder && Input.mouseScrollDelta.y < 0)
            mainCamera.gameObject.transform.localPosition += new Vector3(0, 0, Input.mouseScrollDelta.y);
    }
    
}