﻿using BeeGarden.Model;
using Model.Runtime.Services;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления в юнити менеджера управления всеми цветами
    /// </summary>
    public class FlowerManagerView : MonoBehaviour
    {
        [SerializeField] private Pool flowerPool;
        
        [SerializeField] private FieldView fieldView;

        [SerializeField] private FlowerManager flowerManager;
        
        private void Awake()   
        {
            flowerManager.FlowerBorn += CreateNewFlower;
            StatisticManagerView.GetData += SaveStatistic;
        }

        private void CreateNewFlower(Flower _flower)
        {
            var flowerNew = flowerPool.Pop();
            flowerNew.SetActive(true);
            var flowerDisplay = flowerNew.GetComponent<FlowerView>();
            flowerDisplay.Initialize(_flower);
        }

        private void Update()
        {
            flowerManager.Update(Time.deltaTime);
        }

        public void Initialize(int _fieldStartCount, int _fieldMaxCount,
            float _fieldBornTime, float _flowerMaxHoney, float _flowerStartHoney,
            float _flowerHoneyRegeneration, int _flowerMaxBeeCount)
        {
            flowerManager.Initialize(fieldView.Field, _fieldStartCount, _fieldMaxCount,
                _fieldBornTime, _flowerMaxHoney, _flowerStartHoney,
                _flowerHoneyRegeneration, _flowerMaxBeeCount);
        }
        
        private void SaveStatistic() =>
            AllServices.Container.Single<IStatistic>().SaveData("FlowersCount", flowerManager.GetFlowerCount());

        private void OnDisable()
        {
            flowerManager.FlowerBorn -= CreateNewFlower;
            StatisticManagerView.GetData -= SaveStatistic;
        }
    }
}