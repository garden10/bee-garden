﻿using BeeGarden.Model;
using Model.Runtime.Services;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления в юнити класса управления ульями 
    /// </summary>
    public class BeehiveManagerView : MonoBehaviour
    {
        [SerializeField] private GameObject hive;
        
        [SerializeField] private HiveUI hiveUI;
        
        [SerializeField] private FieldView fieldView;

        [SerializeField] private Pool workerPool;
        
        [SerializeField] private Pool dronePool;
        
        [SerializeField] private BeehiveInitializer beehiveInitializer;

        private void Awake()
        {
            beehiveInitializer.BeeHiveBorn += CreateNewHive;
            StatisticManagerView.GetData += SaveStatistic;
        }

        /// <summary>
        /// Создание нового улья
        /// </summary>
        private void CreateNewHive(Beehive _beeHive, int _number)
        {
            var hiveNew = Instantiate(hive);
            hiveNew.transform.position = _beeHive.Position;
            hiveNew.name = hiveNew.name.Replace("(Clone)","") + _number;
            var beehiveView = hiveNew.GetComponent<BeehiveView>();
            beehiveView.Initialize(_beeHive, hiveUI, workerPool, dronePool);
        }

        /// <summary>
        /// Инициализация через значения, идущие от UI
        /// </summary>
        public void Initialize(int _hiveCount, float _hiveMaxHoney,
            int _hiveMaxBee, int _hiveStartBee, float _hiveBornTime, float _hiveDieTime,
            float _hiveDroneCoefficient, float _hiveDroneGenocideCoefficient, float _beeSpeed,
            float _workerCapacity, float _workerCollectionSpeed, float _workerFlowerDistance)
        {

            beehiveInitializer.Initialize(fieldView.Field, _hiveCount, _hiveMaxHoney,
            _hiveMaxBee, _hiveStartBee, _hiveBornTime, _hiveDieTime,
            _hiveDroneCoefficient, _hiveDroneGenocideCoefficient, _beeSpeed,
            _workerCapacity, _workerCollectionSpeed, _workerFlowerDistance);
        }
        
        private void SaveStatistic() =>
            AllServices.Container.Single<IStatistic>().SaveData("HivesCount", beehiveInitializer.GetHiveCount());

        private void OnDisable()
        {
            beehiveInitializer.BeeHiveBorn -= CreateNewHive;
            StatisticManagerView.GetData -= SaveStatistic;
        }
    }
}