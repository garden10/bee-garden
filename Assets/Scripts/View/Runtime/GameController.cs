﻿using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Переключения из игровой сцены
    /// </summary>
    public class GameController : MonoBehaviour
    {
        public void ToMenu()
        {
            SceneController.Instance.LoadMenu();
        }

        public void ToStatistic()
        {
            SceneController.Instance.LoadStatistic();
        }
    }
}