﻿using BeeGarden.Model;
using Model.Runtime.Services;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс предстаавления улья в юнити
    /// </summary>
    public class BeehiveView : MonoBehaviour
    {
        /// <summary>
        /// Пул рабочих пчел
        /// </summary>
        private Pool workerPool;    
        
        /// <summary>
        /// Пул дронов
        /// </summary>
        private Pool dronePool;

        /// <summary>
        /// Интерфейс улья
        /// </summary>
        private HiveUI hiveUI;

        /// <summary>
        /// Флаг, отвечающий за то, что данный улей готов передавать информацию в интерфейс
        /// </summary>
        private bool transmit;

        private Beehive beehive;
        
        public bool Transmit
        {
            get => transmit;
            set => transmit = value;
        }

        public void Initialize(Beehive _beehive, HiveUI _hiveUI, Pool _workerPool, Pool _dronePool)
        {
            StatisticManagerView.GetData += SaveStatistic;
            
            beehive = _beehive;
            hiveUI = _hiveUI;
            transform.position = beehive.Position;
            workerPool = _workerPool;
            dronePool = _dronePool;
            beehive.OnWorkerOut += CreateNewWorkerView;
            beehive.OnDroneOut += CreateNewDroneView;
            beehive.InitBees();
        }

        public bool CheckFullHoney()
        {
            return beehive.IsFullHoney();
        }
        /// <summary>
        /// Сбор меда по нажатию кнопки в интерфейсе
        /// </summary>
        /// <returns></returns>
        public float GiveHoney()
        {
            return beehive.GiveHoney();
        }
        private void CreateNewWorkerView(Worker _worker)
        {
            var workerNew = workerPool.Pop();
            workerNew.SetActive(true);
            var workerView = workerNew.GetComponent<WorkerView>(); 
            workerView.Initialize(_worker);
        }
        private void CreateNewDroneView(Drone _drone)
        {
            var droneNew = dronePool.Pop();
            droneNew.SetActive(true);
            var droneView = droneNew.GetComponent<DroneView>(); 
            droneView.Initialize(_drone);
        }

        /// <summary>
        /// При нажатии на улей, открывает интерфейс с информацией
        /// </summary>
        private void OnMouseUp()
        {
            hiveUI.gameObject.SetActive(true);
            transmit = true;
        }

        private void Update()
        {
            beehive.Update(Time.deltaTime);
            if (transmit)
            {
                hiveUI.CurrentBeehive = this;
                hiveUI.CurrentHoney = beehive.HoneyInHive;
                hiveUI.HoneyMax = beehive.HoneyMax;
                hiveUI.CurrentBees = beehive.BeeCount;
                hiveUI.BeesMax = beehive.BeeCountMax;
                hiveUI.CurrentWorkers = beehive.WorkersCount;
                hiveUI.CurrentDrones = beehive.DronesCount;
            }
        }

        /// <summary>
        /// Сохранения данных для статистики
        /// </summary>
        private void SaveStatistic()
        {
            AllServices.Container.Single<IStatistic>().SaveData("WorkersCount", beehive.GetWorkersCount());
            AllServices.Container.Single<IStatistic>().SaveData("DronesCount", beehive.GetDronesCount());
        }

        private void OnDisable()
        {
            StatisticManagerView.GetData -= SaveStatistic;
            beehive.OnWorkerOut -= CreateNewWorkerView;
            beehive.OnDroneOut -= CreateNewDroneView;
        }
    }
}