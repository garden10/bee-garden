﻿using System.Collections;
using UnityEngine.SceneManagement;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс состояния
    /// </summary>
    public class GameState : IGameState
    {
        private string sceneName;

        private Scene scene;

        public GameState(string _sceneName)
        {
            sceneName = _sceneName;
        }
        public IEnumerator Enter()
        {
            scene = SceneManager.GetSceneByName(sceneName);
            if(!scene.IsValid() || !scene.isLoaded)
                yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            scene = SceneManager.GetSceneByName(sceneName);
            SceneManager.SetActiveScene(scene);
        }

        public IEnumerator Exit()
        {
            yield return SceneManager.UnloadSceneAsync(sceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
    }
}