﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс основной логики FSM
    /// </summary>
    public class GameFSM
    {
        private Dictionary<StateTransition, IGameState> transitions;
        private IGameState currentState;

        public IEnumerator Initialize( Dictionary<StateTransition, IGameState> _transitions, IGameState _startState)
        {
            currentState = _startState;
            transitions = _transitions;
            yield return currentState.Enter();
        }
        /// <summary>
        /// Запрос на следующее состояние
        /// </summary>
        private IGameState GetNext(GameFSMCommand _command)
        {
            var transition = new StateTransition(currentState, _command);
            IGameState nextState;
            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transition: " + currentState + " -> " + _command);
            return nextState;
        }

        /// <summary>
        /// Переход к следующему состоянию
        /// </summary>
        public IEnumerator MoveNext(GameFSMCommand command)
        {
            yield return currentState.Exit();
            currentState = GetNext(command);
            yield return currentState.Enter();
        }

    }
}