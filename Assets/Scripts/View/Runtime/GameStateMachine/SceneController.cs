﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Контроллер всех сцен
    /// </summary>
    public class SceneController : MonoBehaviour
    {
        private GameFSM gameFsm;
        private static SceneController instance;
        public static SceneController Instance => instance;
        
        [SerializeField] private string menuSceneName;
        [SerializeField] private string gameSceneName;
        [SerializeField] private string statisticSceneName;

        /// <summary>
        /// Инициализация FSM, определение переходов в состояния 
        /// </summary>
        private IEnumerator InitializeFSM()
        {
            var gameState = new GameState(gameSceneName);
            var menuState = new GameState(menuSceneName);
            var statisticState = new GameState(statisticSceneName);
            
            var transitions = new Dictionary<StateTransition, IGameState>
            {
                {new StateTransition(menuState, GameFSMCommand.ToGame), gameState},
                {new StateTransition(menuState, GameFSMCommand.ToStatistic), statisticState},
                {new StateTransition(gameState, GameFSMCommand.ToMenu), menuState},
                {new StateTransition(gameState, GameFSMCommand.ToStatistic), statisticState},
                {new StateTransition(statisticState, GameFSMCommand.ToMenu), menuState},
                {new StateTransition(statisticState, GameFSMCommand.ToGame), gameState}
            };
            gameFsm = new GameFSM();
            yield return gameFsm.Initialize(transitions, menuState);
        }
        private IEnumerator Start()
        {
            instance = this;
            yield return UnloadScenesRoutine();
            yield return InitializeFSM();
        }
        /// <summary>
        /// Загрузка сцены меню
        /// </summary>
        public void LoadMenu()
        {
            StartCoroutine(gameFsm.MoveNext(GameFSMCommand.ToMenu));
        }
        
        /// <summary>
        /// Загрузка сцены игры
        /// </summary>
        public void LoadGame()
        {
            StartCoroutine(gameFsm.MoveNext(GameFSMCommand.ToGame));
        }
        
        /// <summary>
        /// Загрузка сцены статистики
        /// </summary>
        public void LoadStatistic()
        {
            StartCoroutine(gameFsm.MoveNext(GameFSMCommand.ToStatistic));
        }
        /// <summary>
        /// Выгрузка всех сцен в начале игры
        /// </summary>
        private IEnumerator UnloadScenesRoutine()
        {
            var temp = SceneManager.GetSceneByName(gameSceneName);
            if(temp.IsValid())
                yield return SceneManager.UnloadSceneAsync(gameSceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            
            temp = SceneManager.GetSceneByName(statisticSceneName);
            if(temp.IsValid())
                yield return SceneManager.UnloadSceneAsync(statisticSceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            
            temp = SceneManager.GetSceneByName(menuSceneName);
            if(temp.IsValid())
                yield return SceneManager.UnloadSceneAsync(menuSceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
    }
}