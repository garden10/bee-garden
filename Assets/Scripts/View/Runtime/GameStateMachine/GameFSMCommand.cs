﻿namespace BeeGarden.Unity
{
    /// <summary>
    /// Команды для перехода в состояния
    /// </summary>
    public enum GameFSMCommand
    {
        ToGame,
        ToMenu,
        ToStatistic,
    }
}