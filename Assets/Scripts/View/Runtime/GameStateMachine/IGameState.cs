﻿using System.Collections;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Методы, исполняемые в состояниях
    /// </summary>
    public interface IGameState
    {
        /// <summary>
        /// Вход в состояние
        /// </summary>
        IEnumerator Enter();

        /// <summary>
        /// Выход в состояние
        /// </summary>
        IEnumerator Exit();
    }
}