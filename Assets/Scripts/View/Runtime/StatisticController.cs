﻿using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Переключения из сцены статистики
    /// </summary>
    public class StatisticController : MonoBehaviour
    {
        public void ToGame() 
        {
            SceneController.Instance.LoadGame();
        }

        public void ToMenu()
        {
            SceneController.Instance.LoadMenu();
        }
    }
}