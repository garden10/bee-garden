﻿using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Переключения из сцены меню
    /// </summary>
    public class MenuController : MonoBehaviour
    {
        public void ToGame()
        {
            SceneController.Instance.LoadGame();
        }

        public void ToStatistic()
        {
            SceneController.Instance.LoadStatistic();
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}